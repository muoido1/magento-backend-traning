### Unit Three. Rendering

1. Rendering Flow

   1. In the core files, find and print out the layout XML for the product view page. <br>
   https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Plugin/LoggedLayoutXMLPlugin.php <br>
   ![](https://i.imgur.com/Ah9KLZM.png)
   https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/etc/di.xml<br>
   ![](https://i.imgur.com/se3dwZl.png)
   var/log/debug.log <br>
   ![](https://i.imgur.com/lOFt7IN.png)

2. Block Architecture & Life Cycle

   1. Create a block extending AbstractBlock and implement the _toHtml() method. Render that block in the new controller. <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Controller/Index/Index.php <br>
      ![](https://i.imgur.com/m8KUEqg.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Block/Index.php
      ![](https://i.imgur.com/GIIIIJE.png)
      https://muoido.cmmage.app/unit3/index/index <br>
      ![](https://i.imgur.com/4QmeHCr.png)

   2. Create and render a text block in the controller.
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Controller/Index/Index.php <br>
      ![](https://i.imgur.com/m8KUEqg.png)
      https://muoido.cmmage.app/unit3/index/index <br>
      ![](https://i.imgur.com/4QmeHCr.png)

   3. Customize the Catalog\Product\View\Description block, implement the _beforeToHtml() method, and set a custom description for the product here. <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Plugin/CustomProductDescription.php<br>
      ![](https://i.imgur.com/jbXotbO.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/etc/di.xml<br>
      ![](https://i.imgur.com/fVWqiN5.png)
      https://muoido.cmmage.app/olivia-1-4-zip-light-jacket.html<br>
      ![](https://i.imgur.com/U4V9IHZ.png)
   4. Define which template is used in Catalog\Block\Product\View\Attributes.<br>
      bin/magento dev:template-hints:enable <br>
      bin/magento dev:template-hints:disable <br>
      ![](https://i.imgur.com/U5MA36E.png)
      magento/module-catalog/view/frontend/templates/product/view/attribute.phtml<br>

   5. Create a template block and a custom template file for it. Render the block in the controller.<br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Controller/PageCustom/Index.php<br>
      ![](https://i.imgur.com/96vTB8k.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Block/BlockPageCustom.php<br>
      ![](https://i.imgur.com/VBFQsRg.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/PageCustom.phtml<br>
      ![](https://i.imgur.com/yngSXy7.png)
      https://muoido.cmmage.app/unit3/PageCustom/index <br>
      ![](https://i.imgur.com/8EXIj9U.png)
      
   6. Customize the Catalog\Block\Product\View\Description block and assign a custom template to it. <br>

      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Plugin/CustomProductDescription.php <br>
      ![](https://i.imgur.com/DhU66Io.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/etc/di.xml <br>
      ![](https://i.imgur.com/cXw7Xza.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/DescriptionCustom.phtml <br>
      ![](https://i.imgur.com/1YtSQTx.png)
      https://muoido.cmmage.app/juno-jacket.html <br>
      ![](https://i.imgur.com/pGNItv8.png)
      
3. Layout XML: Loading & Rendering

   1. Add a default.xml layout file to the Training_Render module.

      1. Reference the content.top container.
      2. Add a Magento\Framework\View\Element\Template block with a custom template.
         https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/default.xml <br>
         ![](https://i.imgur.com/gBpnRBj.png)
      3. Create your custom template.
         https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/custom_template.phtml <br>
         ![](https://i.imgur.com/j0KE3n7.png)
      4. Check that the template content is visible on every page. ( Kết quả đã bị remove bởi bài 3.6) <br>
        https://muoido.cmmage.app/montana-wind-jacket.html  <br>
         ![](https://i.imgur.com/mQyWQ2Z.png) <br>


   2. Create a new controller action (ex: training_render/layout/onepage).  <br>

      1. For that action, choose a single-column page layout using layout XML.  <br>
         https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/Controller/Layout/Onepage.php <br>
         ![](https://i.imgur.com/VXJiWbF.png)
         https://muoido.cmmage.app/unit3/Layout/Onepage <br>
         ![](https://i.imgur.com/69esvt5.png)
      2. Set a page title using layout XML. <br>
        https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/unit3_layout_onepage.xml <br>
        ![](https://i.imgur.com/aRFKjo6.png)
        https://muoido.cmmage.app/unit3/Layout/Onepage <br>
        ![](https://i.imgur.com/bgwGmib.png)

   3. Add an arguments/argument node to the block. <br>

      1. Set the argument name to background_color. <br>
      2. Set the argument value to lightskyblue. <br>
      3. In the template, add an inline style attribute to a <div> element:  style="background_color: <?= $this->getData('background_color') ?>;" <br>
      4. Confirm that the background color is displayed <br>

      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/unit3_layout_onepage.xml <br>
      ![](https://i.imgur.com/aRFKjo6.png)


      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/Onepage.phtml <br>
      ![](https://i.imgur.com/FyH1ggE.png)
      
      https://muoido.cmmage.app/unit3/Layout/Onepage <br>
      ![](https://i.imgur.com/bgwGmib.png)
         
   4. Change the block color to orange on the product detail pages only. <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/product/breadcrumbs.phtml <br>
      ![](https://i.imgur.com/NBgN2da.png)
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/catalog_product_view.xml <br>
      ![](https://i.imgur.com/FADBIC3.png)
      Background Breadcrumb của product detail đã chuyển sang màu cam:https://muoido.cmmage.app/montana-wind-jacket.html <br>
      ![](https://i.imgur.com/OiPTujj.png)

   5. On category pages, move the exercise block to the bottom of the left column. ( Block đã bị remomve bài 3.6) <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/catalog_category_view.xml <br>
      ![](https://i.imgur.com/WcQFrdG.png)
      ![](https://i.imgur.com/g1H1eaS.png) <br>

   6. On the custom action you just added, remove the custom block from the content.top container. (See Exercise 3.1) <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/default.xml <br>
      ![](https://i.imgur.com/CIQZ5Eq.png) <br>
      block text đã bị remove <br>
      ![](https://i.imgur.com/80gfjw3.png)

   7. Using layout XML, add a new link for the custom page you just created to the set of existing links at the top of every page. <br>
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/default.xml <br>
      ![](https://i.imgur.com/pruTuVn.png)
      ![](https://i.imgur.com/XrMbot6.png)

4. Extra Exercises
   1. Call a CMS block in a custom controller/layout. <br>
      add by layout xml: https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/unit3_layout_onepage.xml <br>
      ![](https://i.imgur.com/WOKFOR5.png)
      Add by template: https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/templates/Onepage.phtml <br>
      ![](https://i.imgur.com/fDpOspZ.png)

      https://muoido.cmmage.app/unit3/Layout/Onepage 
      ![](https://i.imgur.com/NYK4d5C.png)


   2. Create a custom layout for 1 specific category <br>
   https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit3/app/code/Coffeemug/Unit3/view/frontend/layout/catalog_category_view_id_23.xml <br>
   ![](https://i.imgur.com/QZYzGG9.png)
   https://muoido.cmmage.app/women/tops-women/jackets-women.html <br>
   ![](https://i.imgur.com/US46RSQ.png)

### Unit Two. Request Flow

1. Request Flow Overview

    1. Find a place in the code where output is flushed to the browser. Create an extension that captures and logs the file-generated page HTML. (“Flushing output” means a “send” call to the response object.) <br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Observer/LogPageOutput.php<br>
       ![](https://i.imgur.com/AkKQLfQ.png)<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/FirstUnit/etc/events.xml<br>
       ![](https://i.imgur.com/rKePw4J.png)<br>
       var/log/debug.log <br>
       ![](https://i.imgur.com/TUkhhAk.png)

2. Request Routing

    1. Create an extension that logs into the file list of all available routers into a file.<br>
     Không nên override trực tiếp mà thay vào đó xài before, around, hay after vì nếu override trực tiếp thì sẽ ảnh hưởng đến các plugin nếu có sử dụng chung class và khi cập nhật có thể sẽ phát sinh lỗi vì đã override trực tiếp funciton của core.

       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Plugin/FrontController.php <br>
       ![](https://i.imgur.com/SxnCDbb.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/di.xml <br>
       ![](https://i.imgur.com/PLWm7ds.png)
       var/log/debug.log <br>
       ![](https://i.imgur.com/2HA6oAB.png)

    2. Create a new router which “understands” URLs like /test-frontName actionPath-action and converts them to /frontName/actionPath/action
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/frontend/di.xml <br>
       ![](https://i.imgur.com/BBGYnJO.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Controller/Router.php <br>
       ![](https://i.imgur.com/okSMx45.png)

        Hai url đều show thông tin của 1 page: <br>
        https://muoido.cmmage.app/unit2/index/index <br>
        ![](https://i.imgur.com/jY7hnsE.png)

        https://muoido.cmmage.app/testfrontname <br>
        ![](https://i.imgur.com/oFRJWpg.png)

    3. Modify Magento so a “Not Found” page will forward to the home page. <br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/di.xml<br>
       ![](https://i.imgur.com/cFnhOO5.png)<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Plugin/NoroutePlugin.php<br>
       ![](https://i.imgur.com/xSYLcso.png)
       Khi vào 1 url sai sẽ được redirect về home page https://muoido.cmmage.app/asdasdas

3. Working with Controllers

   1. Create a frontend controller that renders “HELLO WORLD” <br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Controller/Index/Index.php<br>
       ![](https://i.imgur.com/kAtU93b.png)<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/frontend/routes.xml<br>
       ![](https://i.imgur.com/0EojORk.png)<br>
       Test url: https://muoido.cmmage.app/unit2/index/index<br>
       ![](https://i.imgur.com/PB1qfRb.png)<br>

   2. Customize the catalog product view controller using plugins and preferences.
		1. Using plugins
			https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/di.xml <br>
			![](https://i.imgur.com/mzVhTSl.png)
			https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Controller/Product/View.php <br>
			![](https://i.imgur.com/xb5S87G.png)
			![](https://i.imgur.com/cXB2Qv8.png)

		2. Using preferences
			https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Preference/CatalogProductPreference.php <br>
			![](https://i.imgur.com/9hp7wkv.png)
			https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/di.xml <br>
			![](https://i.imgur.com/YESO12I.png)
			https://muoido.cmmage.app/proteus-fitness-jackshirt.html <br>
			![](https://i.imgur.com/GK3OJ8w.png)
       
    3. Create an adminhtml controller that allows access only if the GET parameter “secret” is set.
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/adminhtml/routes.xml<br>
       ![](https://i.imgur.com/5y6CIX7.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/adminhtml/menu.xml<br>
       ![](https://i.imgur.com/pCle4xf.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/etc/acl.xml<br>
       ![](https://i.imgur.com/QwgPdus.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Controller/Adminhtml/Index/Index.php<br>
       ![](https://i.imgur.com/UZR7DnH.png)

        Test:<br>
        Success: https://muoido.cmmage.app/admin/coffeemug_unit2/index?secret=my_secret_key<br>
        ![](https://i.imgur.com/JRr8Ga3.png)
        Error: https://muoido.cmmage.app/admin/coffeemug_unit2/index?secret=my_secret_key1<br>
        ![](https://i.imgur.com/uwX7orG.png)

    4. Make the “Hello World” controller you just created redirect to a specific category page.
       <!-- app/code/Coffeemug/Unit2/Controller/Adminhtml/Helloword/Index.php <br>
       ![](https://i.imgur.com/iKvKUt3.png)
       Test: url tự động rediect về admin/catalog/category/edit/id/38/ <br>
       User login: john.smith pass: password123
       https://muoido.cmmage.app/admin/coffeemug_unit2/helloword/ -->
      https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit2/app/code/Coffeemug/Unit2/Controller/Index/Index.php <br>
      ![](https://i.imgur.com/C2uHeQe.png)<br>
      Test url: https://muoido.cmmage.app/unit2/index/index<br>
      Redirect to: https://muoido.cmmage.app/catalog/category/view/id/14 <br>


4. URL Rewrites
    1. Create a URL rewrite for the “Hello World” controller. <br>
    Go to Marketing > URL Rewrites in the admin sidebar <br>
      ![](https://i.imgur.com/hBlQRDU.png)
    Test: https://muoido.cmmage.app/helloworld.html
    ![](https://i.imgur.com/yicfZ56.png)
    

## Lesson 1: Unit One. Preparation & Configuration

### 1. Magento 2 Overview

1. Magento 2 Overview

    1. Install Magento Application on local. <br> Phần này em không làm do đã cài trước đây
    2. Create a category <br>
       Đã thêm category mới https://muoido.cmmage.app/samsung.html<br>

    3. Create a few sample products and experiment with each type: Simple, Configurable, Bundle, Grouped, Downloadable <br>

        Simple product: https://muoido.cmmage.app/simple-product.html<br>

        Configurable product - Sản phẩm tùy biến https://muoido.cmmage.app/configurable-product.html<br>
        Áo có size, màu , cân nặng<br>
        Xe máy chọn niềng , màu<br>

        Grouped product - Sản phẩm nhóm : https://muoido.cmmage.app/grouped-product.html<br>
        Combo điện thoại samsung : điện thoại, sạc dự phòng, ốp lưng, tai nghe ko dây chọn 1 trong số các món cũng được<br>

        Bundle product - set sản phẩm tùy chọn https://muoido.cmmage.app/sprite-yoga-companion-kit.html<br>
        Combo sản phẩm Máy bộ pc: bên tron có ram, cpu , ổ cứng, đủ các bộ phận với thêm vào giỏ hàng được hoặc combo quà trong đó có nhiều loại custom chọn loại trà loại rượu<br>

    4. Create a new module. Make a mistake in its config.
       <br>
       https://gitlab.com/muoido1/magento-backend-traning/-/tree/unit1/app/code/Coffeemug/FirstUnit <br>
       SỬa lỗi trong xml <br>
       ![](https://i.imgur.com/Hwcp7tB.png)
       Sửa module 1 bị lỗi <br>
       Web hiện lỗi <br>
       ![](https://i.imgur.com/vWStR8F.png)

        Create a second module dependent on the first. <br>
        https://gitlab.com/muoido1/magento-backend-traning/-/tree/unit1/app/code/Coffeemug/DependentFirstUnit <br>

2. Development Operations

    1. Mode

        1. Make sure the mode is set to Developer.<br>
           bin/magento deploy:mode:show đã ở mode developer<br>
           ![](https://i.imgur.com/irjb6AG.png)
        2. Then, go into the lib\internal\Magento\Framework\App\Bootstrap.php file and throw an exception in the method, run().<br>
           ![](https://i.imgur.com/V6VXyUV.png)<br>
        3. See whether the exception is displayed on your screen. If it is, you have successfully set the mode. If not, review your steps.<br>
           Sau khi thêm thì web hiện lỗi như hình
           ![](https://i.imgur.com/DL9GYXv.png)

    2. Cache
        1. Under what circumstances will cleaning the var/cache folder not work?<br>
           Khi set cacheable="false" trong xml<br>
           Trang sử dụng cache khác ngoài cache mặc định của magento<br>

3. Dependency Injection & Object Manager

    1. Dependency Injection Go into the Magento core modules folder (app/code/Magento). Open Catalog module; select 5 different lasses from different folders. What kind of a pattern do you notice?<br>
       vendor/magento/module-catalog/Controller/Product/Compare.php<br>
       vendor/magento/module-catalog/Block/Breadcrumbs.php<br>
       vendor/magento/module-catalog/Console/Command/ProductAttributesCleanUp.php<br>
       vendor/magento/module-catalog/Model/CategoryList.php<br>
       vendor/magento/module-catalog/ViewModel/Category/Image.php<br>
       Các class trên sử dụng Dependency Injection<br>

4. Plugin

    1. For the class... Magento\Catalog\Model\Product and the method... getPrice():
       Create a plugin that will modify price (afterPlugin).
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/Plugin/ProductPrice.php <br>
       ![](https://i.imgur.com/K80MHLr.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/etc/frontend/di.xml<br>
       ![](https://i.imgur.com/vjt8RXo.png)
       Giá đã giảm 30U so với giá trong backend là 99$ Link https://muoido.cmmage.app/simple-product.html<br>
       ![](https://i.imgur.com/uxPp3Fn.png)

    2. Customize Magento\Theme\Block\Html\Footer class, to replace the body of the getCopyright() method with your implementation. Retum a hard-coded tring: "Customized copyright!"<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/Plugin/FooterCopy.php<br>
       ![](https://i.imgur.com/zpkVTwc.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/etc/frontend/di.xml<br>
       ![](https://i.imgur.com/m7oTLqL.png)
       Footer đã hiện text custom<br>
       ![](https://i.imgur.com/wT7k23w.png)

    3. Customize Magento\Theme\Block\Html\Breadcrumbs class, addCrumb() method, so that every crumbName is transformed into: $crumbName."(1)"<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/Plugin/BreadcrumdsCustom.php<br>
       ![](https://i.imgur.com/y7WUEBS.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/etc/frontend/di.xml<br>
       ![](https://i.imgur.com/WW0XLYf.png)
       Breadcrumbs của sản phẩm đã được add số 1 sau text Home link: https://muoido.cmmage.app/men/tops-men/jackets-men.html<br>
       ![](https://i.imgur.com/ElfWPw4.png)

5. Events
    1. In your module, create an observer to the event controller_action_predispatch. Get the URL from the request object request->getPathInfo ). Log it into the file.<br>
       https://devdocs.magento.com/guides/v2.3/extension-dev-guide/events-and-observers.html<br>
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/etc/events.xml<br>
       ![](https://i.imgur.com/ZCRoIXR.png)
       https://gitlab.com/muoido1/magento-backend-traning/-/blob/unit1/app/code/Coffeemug/FirstUnit/Observer/LogUrl.php<br>
       ![](https://i.imgur.com/WtKt3WD.png)
       File log var/log/system.log<br>
       ![](https://i.imgur.com/XL9Mq3A.png)<br>

