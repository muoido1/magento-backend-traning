<?php

namespace Coffeemug\FirstUnit\Plugin;

use Magento\Theme\Block\Html\Footer;

class FooterCopy
{
    public function afterGetCopyright(Footer $subject, $result)
    {
        return "Customized copyright!";
    }
}
