<?php

namespace Coffeemug\FirstUnit\Plugin;

use Magento\Theme\Block\Html\Breadcrumbs;

class BreadcrumdsCustom
{
    public function beforeAddCrumb(
        Breadcrumbs $subject,
        $crumbName,
        $crumbInfo
    ) {
        // var_dump($crumbName);
        if ($crumbName == "home") {
            $crumbName = $crumbName . "(1)";
            if (is_object($crumbInfo['label'])) {
                $crumbInfo['label'] = ($crumbInfo['label']->getText() . ' 1');
            } else {
                $crumbInfo['label'] .= ' 1';
            }
        }
        return [$crumbName, $crumbInfo];
    }
}
