<?php

namespace Coffeemug\FirstUnit\Plugin;

use Magento\Catalog\Model\Product;

class ProductPrice
{
    public function afterGetPrice(Product $subject, $result)
    {
        $modifiedPrice = $result - 30;
        return $modifiedPrice;
    }
}
