<?php

/**
 *
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coffeemug\Unit2\Plugin;


use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\App\RouterListInterface;
use Psr\Log\LoggerInterface;

class FrontController
{

    protected $logger;
    protected $routerList;


    public function __construct(LoggerInterface $logger, RouterListInterface $routerList)
    {
        $this->logger = $logger;
        $this->routerList = $routerList;
    }

    public function afterDispatch($subject, $request)
    {
        $routerList = [];
        foreach ($this->routerList as $router) {
            $routerList[] = $router;
        }
        $routerList = array_map(function ($item) {
            return get_class($item);
        }, $routerList);
        $this->logger->info("Magento2 Routers List:" . PHP_EOL . PHP_EOL . implode(PHP_EOL, $routerList));
        return ($request);
    }
}
