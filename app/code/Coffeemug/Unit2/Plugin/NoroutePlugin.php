<?php

namespace Coffeemug\Unit2\Plugin;

use Magento\Cms\Controller\Noroute\Index;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;

class NoroutePlugin
{
    protected $redirectFactory;
    protected $actionFlag;

    public function __construct(
        RedirectFactory $redirectFactory,
        ActionFlag $actionFlag
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->actionFlag = $actionFlag;
    }

    public function aroundExecute(
        Index $subject,
        callable $proceed
    ) {
        $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('/'); // Set the path to the home page or any other desired page

        return $resultRedirect;
    }
}
