<?php

/**
 *
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coffeemug\Unit2\Preference;

use Magento\Catalog\Controller\Product\View;
use \Magento\Framework\Controller\ResultFactory;

/**
 * Class View
 * @package Unit2\CatalogProductPlugin\Controller\Product  extends \Magento\Framework\App\Action\Action

 */
class CatalogProductPreference extends \Magento\Catalog\Controller\Product\View
{
    public function execute()
    {
        $productId = (int) $this->getRequest()->getParam('id');
        if ($productId == 430) {
            $rawResult = $this->resultFactory->create(ResultFactory::TYPE_RAW);
            $rawResult->setContents('Hello world');
            return $rawResult;
        } else {
            return parent::execute();
        }
    }
}
