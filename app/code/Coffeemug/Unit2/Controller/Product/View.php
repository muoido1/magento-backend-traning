<?php

/**
 *
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coffeemug\Unit2\Controller\Product;

use \Magento\Framework\Controller\ResultFactory;

/**
 * Class View
 * @package Unit2\CatalogProductPlugin\Controller\Product
 */
class View extends \Magento\Framework\App\Action\Action
{

    /**
     * @param \Magento\Catalog\Controller\Product\View $controller
     * @param $result
     * @return mixed
     */
    public function afterExecute(
        \Magento\Catalog\Controller\Product\View $controller,
        $result
    ) {


        echo "ONE";
        // exit;
        return $result;
    }
}
