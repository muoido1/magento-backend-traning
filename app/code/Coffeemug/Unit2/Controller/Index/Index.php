<?php

namespace Coffeemug\Unit2\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{
    protected $resultRedirectFactory;

    public function __construct(Context $context, ResultFactory $resultFactory)
    {
        parent::__construct($context);
        $this->resultRedirectFactory = $resultFactory;
    }

    public function execute()
    {
        // Tạo redirect và chuyển hướng đến URL mong muốn
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl('https://muoido.cmmage.app/catalog/category/view/id/14');
        return $resultRedirect;
    }
}
