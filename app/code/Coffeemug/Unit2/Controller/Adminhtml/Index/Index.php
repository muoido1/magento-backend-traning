<?php

/**
 *
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coffeemug\Unit2\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class Index extends Action
{
    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Check if the "secret" parameter is set and allow access accordingly.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        $secret = $this->getRequest()->getParam('secret');
        // Customize the condition to match your requirements
        if ($secret === 'my_secret_key') {
            return true;
        }
        return false;
    }

    /**
     * Execute controller action.
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        // Perform your logic here
        // $this->_redirect('catalog/category/edit/id/38');
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
