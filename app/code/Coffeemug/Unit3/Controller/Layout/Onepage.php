<?php

namespace Coffeemug\Unit3\Controller\Layout;


class Onepage extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\View\LayoutInterface $layout

    ) {
        $this->_pageFactory = $pageFactory;
        $this->layout = $layout;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_pageFactory->create();

        $resultPage->getConfig()->setPageLayout('1column');
        // Add the CMS block to the layout
        // $cmsBlockId = 'extra-exercises-3';
        // $cmsBlock = $this->layout->createBlock('Magento\Cms\Block\Block')->setBlockId($cmsBlockId);
        // var_dump($cmsBlock->toHtml());
        // var_dump("aasèwefgwefwefwfwfewefwe");
        // $resultPage->getLayout()->getBlock('block_identifier')->append($cmsBlock);
        // var_dump($resultPage->getLayout()->getBlock('block_identifier'));


        return $resultPage;
    }
}
