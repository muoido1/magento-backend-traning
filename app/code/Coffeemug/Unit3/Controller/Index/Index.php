<?php

namespace Coffeemug\Unit3\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        // $layout = $this->_view->getLayout();
        $layout = $this->_pageFactory->create()->getLayout();
        $block = $layout->createBlock('Coffeemug\Unit3\Block\Index');

        // Create text block 
        $block2 = $layout->createBlock(\Magento\Framework\View\Element\Text::class);
        $block2->setText('This is the content of the text block.');

        // $resultPage->getLayout()->setChild('content', $block->getNameInLayout(), $block2->toHtml());

        $this->getResponse()->appendBody($block->_toHtml());
        $this->getResponse()->appendBody($block->helloBlock());
        $this->getResponse()->appendBody($block2->toHtml());
    }
}
