<?php

namespace Coffeemug\Unit3\Controller\PageCustom;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        // return $this->_pageFactory->create();
        $block = $this->_pageFactory->create()->getLayout()->createBlock('Coffeemug\Unit3\Block\BlockPageCustom');
        $block->setTemplate('PageCustom.phtml');
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $result->setContents($block->toHtml());
        return $result;
    }
}
