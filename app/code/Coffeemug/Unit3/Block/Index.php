<?php

namespace Coffeemug\Unit3\Block;

class Index extends \Magento\Framework\View\Element\AbstractBlock
{
    public function _toHtml()
    {
        return "<h1><b>Hello world from block!</b></h1>";
    }

    public function helloBlock()
    {
        return "<h2><b>helloBlock from block!</b></h2>";
    }
}
