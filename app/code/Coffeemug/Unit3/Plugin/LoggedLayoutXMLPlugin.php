<?php

namespace Coffeemug\Unit3\Plugin;

use Magento\Framework\View\LayoutInterface;
use Psr\Log\LoggerInterface;

class LoggedLayoutXMLPlugin
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * LogPageOutput constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }
    public function afterGenerateXml(LayoutInterface $subject, $result)
    {
        // var_dump();
        $this->_logger->info("XML product" . PHP_EOL . $subject->getXmlString() . PHP_EOL);

        return $result;
    }
}
