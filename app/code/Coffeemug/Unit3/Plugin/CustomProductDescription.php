<?php

namespace Coffeemug\Unit3\Plugin;

use Psr\Log\LoggerInterface;
use Magento\Catalog\Block\Product\View\Description;

class CustomProductDescription
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * LogPageOutput constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }
    public function beforeToHtml(Description $description)
    {
        // var_dump($description->getProduct()->getEntityId());
        if ($description->getProduct()->getEntityId() == 1380) {
            $description->setTemplate('Coffeemug_Unit3::DescriptionCustom.phtml');
        }

        if ($description->getProduct()->getEntityId() == 1396) {
            $description->getProduct()->setDescription('Custom description!');
        }
    }
}
